---
title: 'Final Project:  375 Top Video Games / Over 3.5 Million Sold'
author: "Jaryt Salvo"
output: html_document
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message = FALSE,
                      warning = FALSE)
```
Here is a brief description of the project, the final assignment of this class. 

1.  You find an interesting dataset that you are interested in learning about.  It should contain at least 50 observations with at least 3 variables (one of the variables can be categorical).

2.  Think of a list of questions that you’d like to answer through an exploratory analysis.

3.  You apply some of the EDA methods we’ve learned to explore the data.

4.  The focus on your exploration is not on the methods but rather on the interpretation of the methods in the context of your data.

5.  You prepare a Google Slides presentation that describes your analysis.  You can give your R work in an appendix, but the presentation should summarize what you’ve learned.

library(LearnEDAfunctions)
library(car)
library(vcd)

games <- read.csv('C:/Users/super/Documents/MSA/Fall 2020/MSA 5470 - Exploratory Data Analysis/Final Project/video_games_sales_full.csv')

games.yr <- games[order(games$Year_of_Release, games$Platform), ]
games.yr

games.yr$tser <- seq(1,376)

plot(games.yr$tser, games.yr$Global_Sales)

games$na.per <- games$NA_Sales/games$Global_Sales
games$eu.per <- games$EU_Sales/games$Global_Sales
games$jp.per <- games$JP_Sales/games$Global_Sales
games$ot.per <- games$Other_Sales/games$Global_Sales


with(games.yr, plot(tser, Global_Sales))

factor(games$Platform)
library(plyr) 

count(games, c("Platform", "Genre"))
count(games, 'Genre')
count(games, 'Platform')

plat.2600 <- subset(games, Platform == '2600')
plat.3ds <- subset(games, Platform == '3DS')
plat.ds <- subset(games, Platform == 'DS')
plat.gb <- subset(games, Platform == 'GB')
plat.gba <- subset(games, Platform == 'GBA')
plt.gc <- subset(games, Platform == 'GC')
plt.gen <- subset(games, Platform == 'GEN')
plat.n64 <- subset(games, Platform == 'N64')
plat.nes <- subset(games, Platform == 'NES')
plat.pc <- subset(games, Platform == 'PC')
plat.ps <- subset(games, Platform == 'PS')
plat.ps2 <- subset(games, Platform == 'PS2')
plat.ps3 <- subset(games, Platform == 'PS3')
plat.ps4 <- subset(games, Platform == 'PS4')
plat.psp <- subset(games, Platform == 'PSP')
plat.snes <- subset(games, Platform == 'SNES')
plat.wii <- subset(games, Platform == 'Wii')
plat.wiiU <- subset(games, Platform == 'WiiU')
plat.x360 <- subset(games, Platform == 'X360')
plat.xb <- subset(games, Platform == 'XB')
plat.xone <- subset(games, Platform == 'XOne')

factor(games$Year_of_Release)

#YEARS SUBSETTING
year.8090 <- subset(games, Year_of_Release >= 1980 & Year_of_Release < 1990)
year.9000 <- subset(games, Year_of_Release >= 1990 & Year_of_Release < 2000)
year.0010 <- subset(games, Year_of_Release >= 2000 & Year_of_Release < 2010)
year.1020 <- subset(games, Year_of_Release >= 2010 & Year_of_Release < 2020)

year.1 <- subset(games, Year_of_Release >= 1980 & Year_of_Release <= 1985)
year.2 <- subset(games, Year_of_Release > 1985 & Year_of_Release <= 1990)
year.3 <- subset(games, Year_of_Release > 1990 & Year_of_Release <= 1995)
year.4 <- subset(games, Year_of_Release > 1995 & Year_of_Release <= 2000)
year.5 <- subset(games, Year_of_Release > 2000 & Year_of_Release <= 2005)
year.6 <- subset(games, Year_of_Release > 2005 & Year_of_Release <= 2010)
year.7 <- subset(games, Year_of_Release > 2010 & Year_of_Release <= 2015)

factor(games$Genre)

gen.act <- subset(games, Genre == 'Action')
gen.adv <- subset(games, Genre == 'Adventure')
gen.fgt <- subset(games, Genre == 'Fighting')
gen.mis <- subset(games, Genre == 'Misc')
gen.plat <- subset(games, Genre == 'Platform')
gen.puz <- subset(games, Genre == 'Puzzle')
gen.rac <- subset(games, Genre == 'Racing')
gen.rpg <- subset(games, Genre == 'Role-Playing')
gen.sho <- subset(games, Genre == 'Shooter')
gen.sim <- subset(games, Genre == 'Simulation')
gen.spt <- subset(games, Genre == 'Sports')
gen.str <- subset(games, Genre == 'Strategy')

bins1 <- seq(1982, 2018, 6)
bins2 <- seq(1980, 2015, 5)

bins1
bins2

with(games, plot(Year_of_Release, Global_Sales, ylim = c(4,40)))
with(games, hist(Global_Sales, xlab = 'Global Sales (in Millions)'))

#One point removed from ylim argument
boxplot(year.1$Global_Sales, year.2$Global_Sales, year.3$Global_Sales, year.4$Global_Sales, year.5$Global_Sales,year.6$Global_Sales, year.7$Global_Sales, ylim = c(4,40), main = 'Global Sales in 5-Year Increments \n from 1980-2015', horizontal=T)

#One point removed from ylim argument
boxplot(gen.act$Global_Sales, gen.adv$Global_Sales, gen.fgt$Global_Sales, gen.mis$Global_Sales, gen.plat$Global_Sales, gen.puz$Global_Sales, gen.rac$Global_Sales, gen.rpg$Global_Sales, gen.sho$Global_Sales, gen.sim$Global_Sales, gen.spt$Global_Sales, gen.str$Global_Sales, ylim = c(4,40), horizontal=T)

#One point removed from ylim argument
boxplot(gen.act$NA_Sales, gen.adv$NA_Sales, gen.fgt$NA_Sales, gen.mis$NA_Sales, gen.plat$NA_Sales, gen.puz$NA_Sales, gen.rac$NA_Sales, gen.rpg$NA_Sales, gen.sho$NA_Sales, gen.sim$NA_Sales, gen.spt$NA_Sales, gen.str$NA_Sales, ylim = c(0,30), horizontal=T)

#One point removed from ylim argument
boxplot(gen.act$EU_Sales, gen.adv$EU_Sales, gen.fgt$EU_Sales, gen.mis$EU_Sales, gen.plat$EU_Sales, gen.puz$EU_Sales, gen.rac$EU_Sales, gen.rpg$EU_Sales, gen.sho$EU_Sales, gen.sim$EU_Sales, gen.spt$EU_Sales, gen.str$EU_Sales, ylim = c(0,13), horizontal=T)

boxplot(gen.act$JP_Sales, gen.adv$JP_Sales, gen.fgt$JP_Sales, gen.mis$JP_Sales, gen.plat$JP_Sales, gen.puz$JP_Sales, gen.rac$JP_Sales, gen.rpg$JP_Sales, gen.sho$JP_Sales, gen.sim$JP_Sales, gen.spt$JP_Sales, gen.str$JP_Sales, horizontal=T)

#Three point removed from ylim argument
boxplot(gen.act$Other_Sales, gen.adv$Other_Sales, gen.fgt$Other_Sales, gen.mis$Other_Sales, gen.plat$Other_Sales, gen.puz$Other_Sales, gen.rac$Other_Sales, gen.rpg$Other_Sales, gen.sho$Other_Sales, gen.sim$Other_Sales, gen.spt$Other_Sales, gen.str$Other_Sales, ylim = c(0,4), horizontal=T)

boxplot(Global_Sales~Year_of_Release, horizontal=T, subset = Year_of_Release == (c(1980:1989)), data=games, xlab = "Global Sales (in Millions)", ylab = "Year of Release")
boxplot(Global_Sales~Platform, horizontal=T, data=games, xlab = "Global Sales (in Millions)", ylab = "Platform")
boxplot(Global_Sales~Genre, horizontal=T, data=games, subset = 'Year_of_Release', xlab = "Global Sales (in Millions)", ylab = "Genre")

boxplot(year.8090$Global_Sales, year.9000$Global_Sales, year.0010$Global_Sales, year.1020$Global_Sales, ylim = c(4,40), horizontal=T)

#froot flog
froot <- function(p) sqrt(p) - sqrt(1- p)
flog <- function(p) log(p) - log(1 - p)

boxplot(games$na.per, games$eu.per, games$jp.per, games$ot.per, horizontal=T)
boxplot(froot(games$na.per), froot(games$eu.per), froot(games$jp.per), froot(games$ot.per), horizontal=T)
boxplot(flog(games$na.per), flog(games$eu.per), flog(games$jp.per), flog(games$ot.per), horizontal=T)

summary(games$na.per)
summary(games$eu.per)
summary(games$jp.per)
summary(games$ot.per)

summary(froot(games$na.per))
summary(froot(games$eu.per))
summary(froot(games$jp.per))
summary(froot(games$ot.per))

summary(flog(games$na.per+.0000001))
summary(flog(games$eu.per+.0000001))
summary(flog(games$jp.per+.0000001))
summary(flog(games$ot.per+.0000001))

#spreadLevel
#Global Sales
with(games, spreadLevelPlot(Global_Sales, Genre, main='spreadLevelPlot Global Sales'))
with(games, spreadLevelPlot(Global_Sales^-2, Genre, main='spreadLevelPlot Global Sales Transformed'))
#One point removed from ylim argument
boxplot(Global_Sales ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", ylim = c(4,41), xlab="Global Sales")
boxplot(Global_Sales^-2 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed Global Sales")

#NA Sales
with(games, spreadLevelPlot(NA_Sales, Genre, main='spreadLevelPlot NA Sales'))
with(games, spreadLevelPlot(NA_Sales^-.5, Genre, main='spreadLevelPlot NA Sales Transformed'))
#One point removed from ylim argument
boxplot(NA_Sales ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", ylim = c(0,41), xlab="NA Sales")
boxplot(NA_Sales^-.5 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed NA Sales")

#EU Sales
with(games, spreadLevelPlot(EU_Sales, Genre, main='spreadLevelPlot EU Sales'))
with(games, spreadLevelPlot(EU_Sales^0.5, Genre, main='spreadLevelPlot EU Sales Transformed'))
#One point removed from ylim argument
boxplot(EU_Sales ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", ylim = c(0,13), xlab="EU Sales")
boxplot(EU_Sales^0.5 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed EU Sales")

#JP Sales
with(games, spreadLevelPlot(JP_Sales, Genre, main='spreadLevelPlot Japan Sales'))
with(games, spreadLevelPlot(JP_Sales^0.5, Genre, main='spreadLevelPlot Japan Sales Transformed'))
boxplot(JP_Sales ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="JP Sales")
boxplot(JP_Sales^0.5 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed JP Sales")

#Other Sales
with(games, spreadLevelPlot(Other_Sales, Genre, main='spreadLevelPlot Other Sales'))
with(games, spreadLevelPlot(Other_Sales^0.5, Genre, main='spreadLevelPlot Other Sales Transformed'))
#Three point removed from ylim argument
boxplot(Other_Sales ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", ylim = c(0,4), xlab="Other Sales")
boxplot(Other_Sales^0.5 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed Other Sales")


#NA as %
with(games, spreadLevelPlot(na.per, Genre, main='spreadLevelPlot NA Sales percentage'))
with(games, spreadLevelPlot(na.per^2, Genre, main='spreadLevelPlot NA Sales percentage Transformed'))
boxplot(na.per ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="NA % Sales")
boxplot(na.per^2 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed NA % Sales")

#EU as %
with(games, spreadLevelPlot(eu.per, Genre, main='spreadLevelPlot EU Sales percentage'))
with(games, spreadLevelPlot(eu.per^1.673908, Genre, main='spreadLevelPlot EU Sales percentage Transformed'))
boxplot(eu.per ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="EU % Sales")
boxplot(eu.per^1.673908 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed EU % Sales")

#JP as %
with(games, spreadLevelPlot(jp.per, Genre, main='spreadLevelPlot Japan Sales percentage'))
with(games, spreadLevelPlot(jp.per^0.5, Genre, main='spreadLevelPlot Japan Sales percentage Transformed'))
boxplot(jp.per ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="JP % Sales")
boxplot(jp.per^0.5 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed JP % Sales")

#Other as %
with(games, spreadLevelPlot(ot.per, Genre, main='spreadLevelPlot Other Sales percentage'))
with(games, spreadLevelPlot(ot.per^0.7811404, Genre, main='spreadLevelPlot Other Sales percentage Transformed'))
boxplot(ot.per ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Other % Sales")
boxplot(ot.per^0.7811404 ~ Genre, data=games,
        horizontal=TRUE,
        ylab="Genre", xlab="Reexpressed Other % Sales")


with(games, spreadLevelPlot(NA_Sales, Platform, main='spreadLevelPlot NA Platform'))
with(games, spreadLevelPlot(NA_Sales^0.4284455, Platform, main='spreadLevelPlot NA Platform'))

with(games, spreadLevelPlot(EU_Sales, Platform, main='spreadLevelPlot EU Platform'))
with(games, spreadLevelPlot(EU_Sales^-0.3554103, Platform, main='spreadLevelPlot EU Platform'))

with(games, spreadLevelPlot(JP_Sales, Platform, main='spreadLevelPlot Japan Platform'))
with(games, spreadLevelPlot(JP_Sales, Platform, main='spreadLevelPlot Japan Platform'))

with(games, spreadLevelPlot(Other_Sales, Platform, main='spreadLevelPlot Other Platform'))
with(games, spreadLevelPlot(Other_Sales^-0.2838131, Platform, main='spreadLevelPlot Other Platform'))


# Global Sales Plot Years
with(games, plot(Year_of_Release, Global_Sales, main = 'Global Game Sales of Top 375 \n Selling Titles from 1980-2016', ylab = 'Global Sales (in Millions)'))

#Time Series Attempts
plot(games.yr$tser, games.yr$Global_Sales)

smooth.3R <- smooth(games.yr$Global_Sales, kind="3R")

with(games.yr,
     plot(smooth.3R,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="3R Smooth"))

smooth.3RSS <- smooth(games.yr$Global_Sales, kind="3RSS")

with(games.yr,
     plot(smooth.3RSS,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="3RSS Smooth"))

with(games.yr,
     plot(t,type="l",col="green",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="no-Smooth, 3R, & 3RSS Smooths"))
with(games.yr,
     lines(smooth.3R,type="l",col="blue",lwd=2,))
with(games.yr,
     lines(smooth.3RSS,col="red",lwd=2))
legend("topleft",
       legend=c("3R", "3RSS", "No-Smooth"),
       lty=1, col=c("blue", "red", "green"))

smooth.3RSSH <- han(smooth.3RSS)
with(games.yr,
     plot(tser, smooth.3RSSH, type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="3RRSH Smooth"))

#Try 2
t <- tapply(games$Global_Sales, games$Year_of_Release, FUN=sum)
lm(t~seq(1:36))

plot(tapply(games$Global_Sales, games$Year_of_Release, FUN=sum), main = 'Global Game Sales from 1980-2016', ylab = 'Sum of Global Sales (in Millions)')
abline(a=-4.038, b=4.402)

smooth.3R2 <- smooth(t, kind="3R")

with(games,
     plot(smooth.3R2,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="3R Smooth"))

smooth.3RSS2 <- smooth(t, kind="3RSS")

with(games,
     plot(smooth.3RSS2,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="3RSS Smooth"))

with(games,
     plot(t,type="l",col="green",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Sales (in Millions)",
          main="No-Smooth, 3R, & 3RSS Smooths"))
with(games,
     lines(smooth.3R2,type="l",col="blue",lwd=2,))
with(games,
     lines(smooth.3RSS2,col="red",lwd=2))
legend("topleft",
       legend=c("3R", "3RSS", "No-Smooth"),
       lty=1, col=c("blue", "red", "green"))

#Try 3
t2 <- tapply(games$Global_Sales, games$Year_of_Release, FUN=length)
lm(t2~seq(1:36))

plot(tapply(games$Global_Sales, games$Year_of_Release, FUN=length), main = 'Global Game Sales from 1980-2016', ylab = 'Number of Titles in Global Sales')
abline(a=-1.1698, b=.6278)

smooth.3R3 <- smooth(t2, kind="3R")

with(games,
     plot(smooth.3R3,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Count Sales",
          main="3R Smooth"))

smooth.3RSS3 <- smooth(t2, kind="3RSS")

with(games,
     plot(smooth.3RSS3,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Count Sales",
          main="3RSS Smooth"))

with(games,
     plot(t2,type="l",col="green",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Global Count Sales",
          main="No-Smooth, 3R, & 3RSS Smooths"))
with(games,
     lines(smooth.3R3,type="l",col="blue",lwd=2,))
with(games,
     lines(smooth.3RSS3,col="red",lwd=2))
legend("topleft",
       legend=c("3R", "3RSS", "No-Smooth"),
       lty=1, col=c("blue", "red", "green"))

#Try 3
t3 <- tapply(games$Global_Sales, games$Year_of_Release, FUN=mean)
lm(t3~seq(1:36))

plot(tapply(games$Global_Sales, games$Year_of_Release, FUN=mean), main = 'Global Game Sales from 1980-2016', ylab = 'Mean Global Sales (in Millions)')
abline(a=8.92842, b=-0.06591)

smooth.3R4 <- smooth(t3, kind="3R")

with(games,
     plot(smooth.3R4,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Mean Global Sales (in Millions)",
          main="3R Smooth"))

smooth.3RSS4 <- smooth(t3, kind="3RSS")

with(games,
     plot(smooth.3RSS4,type="l",col="red",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Mean Global Sales (in Millions)",
          main="3RSS Smooth"))

with(games,
     plot(t3,type="l",col="green",lwd=2,
          xlab="Year starting from 1980 to 2016",ylab="Mean Global Sales (in Millions)",
          main="No-Smooth, 3R, & 3RSS Smooths"))
with(games,
     lines(smooth.3R4,type="l",col="blue",lwd=2,))
with(games,
     lines(smooth.3RSS4,col="red",lwd=2))
legend("topright",
       legend=c("3R", "3RSS", "No-Smooth"),
       lty=1, col=c("blue", "red", "green"))

#Rough Plot
FinalRough <- t - smooth.3RSS2
plot(seq(1:36), FinalRough,
     pch=19, cex=1.3, main = 'Rough of Sum 3RSS Smooth', xlab="Year starting from 1980 to 2016", ylab="ROUGH")
abline(h=0, lwd=3, col="blue")

FinalRough2 <- t2 - smooth.3RSS3
plot(seq(1:36), FinalRough2,
     pch=19, cex=1.3, main = 'Rough of Count 3RSS Smooth', xlab="Year starting from 1980 to 2016", ylab="ROUGH")
abline(h=0, lwd=3, col="blue")

FinalRough3 <- t3 - smooth.3RSS4
plot(seq(1:36), FinalRough3,
     pch=19, cex=1.3, main = 'Rough of Mean 3RSS Smooth', xlab="Year starting from 1980 to 2016", ylab="ROUGH")
abline(h=0, lwd=3, col="blue")

#New Data
NA.Sales <- tapply(games$NA_Sales, games$Year_of_Release, FUN=sum)
EU.Sales <- tapply(games$EU_Sales, games$Year_of_Release, FUN=sum)
JP.Sales <- tapply(games$JP_Sales, games$Year_of_Release, FUN=sum)
Other.Sales <- tapply(games$Other_Sales, games$Year_of_Release, FUN=sum)

sales.85 <- colSums(year.1[,c(6:9)],na.rm=T)
sales.85
sales.90 <- colSums(year.2[,c(6:9)],na.rm=T)
sales.95 <- colSums(year.3[,c(6:9)],na.rm=T)
sales.00 <- colSums(year.4[,c(6:9)],na.rm=T)
sales.05 <- colSums(year.5[,c(6:9)],na.rm=T)
sales.10 <- colSums(year.6[,c(6:9)],na.rm=T)
sales.15 <- colSums(year.7[,c(6:9)],na.rm=T)

test <- cbind(NA.Sales,EU.Sales,JP.Sales,Other.Sales)
test

test2 <- rbind(sales.85,sales.90,sales.95,sales.00,sales.05,sales.10,sales.15)
test2

additive.fit <- medpolish(test2)
additive.fit

Row.Part <- with(additive.fit, row + overall)
Col.Part <- additive.fit$col
plot2way(Row.Part, Col.Part, dimnames(test2)[[1]], dimnames(test2)[[2]])

log.test <- log10(test2+1)

mult.fit <- medpolish(log.test,na.rm=T)
mult.fit

mult.Row.Part <- with(mult.fit, row + overall)
mult.Col.Part <- mult.fit$col
plot2way(mult.Row.Part, mult.Col.Part, dimnames(log.test)[[1]], dimnames(log.test)[[2]])

COMMON <- 10 ^ mult.fit$overall
ROW <- 10 ^ mult.fit$row
COL <- 10 ^ mult.fit$col
RESIDUAL <- 10 ^ mult.fit$residual
COMMON
ROW
COL
RESIDUAL

#Additive Fit Check
Year <- (c(seq(1985, 2015, by=5)))
#par(mfrow=c(2, 1))
lm(additive.fit$row~seq(1985, 2015, 5))

plot(Year, additive.fit$row,
     ylab="Row Effect", pch=19,
     main="Plot of Year Effects")
abline(a=-13207.712, b=6.607, col='blue')

hist(additive.fit$residuals)
shapiro.test(additive.fit$residuals)

#Mult Fit Check
lm(mult.fit$row~seq(1985, 2015, 5))

plot(Year, mult.fit$row,
     ylab="Row Effect", pch=19,
     main="Plot of Year Effects")
abline(a=-81.79334, b=0.04083, col='red')

hist(mult.fit$residuals)
shapiro.test(mult.fit$residuals)

fec <- COMMON * ROW

fec[['sales.90']] / fec[['sales.85']]
fec[['sales.95']] / fec[['sales.90']]
fec[['sales.00']] / fec[['sales.95']]
fec[['sales.05']] / fec[['sales.00']]
fec[['sales.10']] / fec[['sales.05']]
fec[['sales.15']] / fec[['sales.10']]
fec[['sales.15']] / fec[['sales.95']]
fec[['sales.10']] / fec[['sales.85']]

COL
COL[['NA_Sales']] / COL[['EU_Sales']]
COL[['NA_Sales']] / COL[['JP_Sales']]
COL[['NA_Sales']] / COL[['Other_Sales']]
COL[['EU_Sales']] / COL[['JP_Sales']]

#Extended Fit
games.x <- test2[order(additive.fit$row), ]
add.fit <- medpolish(games.x)
add.fit

COMMON2 <- add.fit$overall
ROW2 <- add.fit$row
COL2 <- add.fit$col
RESIDUAL2 <- add.fit$residual
COMMON2
ROW2
COL2
RESIDUAL2

cv <- with(add.fit, outer(row, col, "*") / overall)
plot(as.vector(cv), as.vector(add.fit$residuals), main = "Estimated k value",
     xlab = "COMPARISON VALUES", ylab = "RESIDUALS")

lm(as.vector(add.fit$residuals)~as.vector(cv))

plot(as.vector(cv),
     as.vector(add.fit$residuals) - 0.7807 * as.vector(cv),
     xlab = "COMPARISON VALUES", ylab = "RESIDUAL FROM FIT",
     main="Checking Suitability of Line Fit")
abline(h=0, col="red")
